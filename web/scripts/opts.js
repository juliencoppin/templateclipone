var DTOpts = {
    search : {
        caseInsensitive: true
    },
    language: {},
    columnDefs: [],
    order: [],
    createdRow: function(row) {
        $(row).find('.tooltips').tooltip();
    },
    // drawCallback: function(settings) {
    //     var api = this.api();
    //     var items = api.cells().nodes().to$().find('.tooltips');
    //     if (items.length > 0) {
    //         items.tooltip("destroy");
    //         items.tooltip();
    //     }
    // },
    scrollX: true
};

if (fullLocale !== null) {
    DTOpts.language = {
        url: "//cdn.datatables.net/plug-ins/1.10.10/i18n/" + fullLocale + ".json"
    };
}

var Select2Opts = {
    allowClear: true,
    language: smallLocale
};

var DatepickerOpts = {
    autoclose: true,
    clearBtn: true,
    format: 'dd-mm-yyyy',
    language: smallLocale,
    weekStart: 1,
    todayHighlight: true
};