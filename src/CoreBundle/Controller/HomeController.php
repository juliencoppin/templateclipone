<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 16/01/2018
 * Time: 11:35
 */

namespace CoreBundle\Controller;


use JulienCoppin\CustomBundle\Controller\MasterController;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends MasterController
{
    public function dispatchAction()
    {
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->forward('AdminBundle:Webmaster:index');
        }

        return new Response('DO SOMETHING');
    }
}