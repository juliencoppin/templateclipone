<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 16/01/2018
 * Time: 11:08
 */

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * Class User
 * @package CoreBundle\Entity
 *
 * @ORM\Table(name="User")
 * @ORM\Entity()
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="UserID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
}